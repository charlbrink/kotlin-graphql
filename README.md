##Overview
See blog post https://auth0.com/blog/building-graphql-apis-with-kotlin-spring-boot-and-mongodb/

##Running Application
gradlew bootRun

Open browser at http://localhost:9000/graphiql
See sample requests below.
With OAuth see kotlin-graphql.postman_collection.json

##Create Snack
mutation {
  newSnack(name: "French Fries", amount: 40.5) {
    id
    name
    amount
  }
}

##Create Review
mutation {
    newReview(snackId:"e880285a-1d7f-48da-a522-e1d06fb0d4a4",
    text: "Awesome snack!", rating:5
    ){
        snackId, text, rating
    }
}

##Query Snacks

query { 
    snacks {
        id 
        name, reviews { text, rating } } }